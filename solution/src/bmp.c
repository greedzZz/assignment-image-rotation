#include "bmp.h"

static const uint16_t TYPE = 19778;
static const uint16_t BITS_PER_PIXEL = 24;
static const uint32_t HEADER_SIZE = 54;
static const uint32_t DIB_SIZE = 40;
static const uint16_t PLANES = 1;

struct __attribute__((packed)) bmp_header
{
        uint16_t bfType;
        uint32_t  bfileSize;
        uint32_t bfReserved;
        uint32_t bOffBits;
        uint32_t biSize;
        uint32_t biWidth;
        uint32_t  biHeight;
        uint16_t  biPlanes;
        uint16_t biBitCount;
        uint32_t biCompression;
        uint32_t biSizeImage;
        uint32_t biXPelsPerMeter;
        uint32_t biYPelsPerMeter;
        uint32_t biClrUsed;
        uint32_t  biClrImportant;
};

static uint32_t get_img_size(uint32_t const width, uint32_t const height, uint32_t const padding) {
    return width * height * 3 + padding * height;
}

static uint32_t get_padding(uint32_t const width) {
    if (width % 4 == 0) return 0;
    return ((width * 3 / 4) + 1) * 4 - width * 3;
}

static enum status read_header(FILE* const file, struct bmp_header* const header) {
    if (fread(header, sizeof(struct bmp_header), 1, file) == 1) return OK;
    return READ_ERR;
}

static enum status validate_header (struct bmp_header* const header) {
    if (header->bfType != TYPE) {
        return READ_INVALID_TYPE;
    }
    if (header->biBitCount != BITS_PER_PIXEL) {
        return READ_INVALID_BPP;
    }
    if (header->bOffBits != HEADER_SIZE) {
        return READ_INVALID_HEADER_SIZE;
    }
    return OK;
}

static enum status read_data(FILE* const file, struct image* const img) {
    const uint32_t width = (uint32_t) img->width;
    const uint32_t height = (uint32_t) img->height;
    const uint32_t padding = get_padding(width);
    if (width == 0 || height == 0) return READ_INVALID_SIZE;
    struct pixel* data = malloc(sizeof(struct pixel) * width * height);
    if (!data) return READ_MALLOC_ERR;
    struct pixel* ptr = data;
    for (size_t i = 0; i < height; i++) {
        size_t pixels = fread(ptr, sizeof(struct pixel), width, file);
        if (pixels != width) {
          free(ptr);
          return READ_ERR;
        }
        size_t skipped = fseek(file, padding, SEEK_CUR);
        if (skipped != 0) {
          free(ptr);
          return READ_FSEEK_ERR;
        }
        ptr += width;
    }
    img->data = data;
    return OK;
}

static enum status write_header(FILE* const file, uint32_t const width, uint32_t const height, uint32_t const padding) {
    const uint32_t img_size = get_img_size(width, height, padding);
    struct bmp_header header = {
        .bfType = TYPE,
        .bfileSize = HEADER_SIZE + img_size,
        .bfReserved = 0,
        .bOffBits = HEADER_SIZE,
        .biSize = DIB_SIZE,
        .biWidth = width,
        .biHeight = height,
        .biPlanes = PLANES,
        .biBitCount = BITS_PER_PIXEL,
        .biCompression = 0,
        .biSizeImage = img_size,
        .biXPelsPerMeter = 0,
        .biYPelsPerMeter = 0,
        .biClrUsed = 0,
        .biClrImportant = 0
    };   
    size_t count = fwrite(&header, sizeof(struct bmp_header), 1, file);
    if (count == 1) return OK;
    return WRITE_ERR;
}

static enum status write_data(FILE* const file, uint32_t const width, uint32_t const height, uint32_t const padding, struct pixel* ptr) {
    for (size_t i = 0; i < height; i++) {
        size_t pixels = fwrite(ptr, sizeof(struct pixel), width, file);
        if (pixels != width) return WRITE_ERR;
        size_t paddings = fwrite(ptr, padding, 1, file);
        if (paddings != 1) return WRITE_ERR;
        ptr += width;  
    }
    return OK;
}

enum status from_bmp(FILE* const in, struct image* const img ) {
    struct bmp_header header;
    enum status header_status = read_header(in, &header);
    if (header_status != OK) return header_status;
    enum status validation_status = validate_header(&header);
    if (validation_status != OK) return validation_status;
    img->width = (uint64_t) header.biWidth;
    img->height = (uint64_t) header.biHeight;
    enum status data_status = read_data(in, img);
    return data_status;
}

enum status to_bmp( FILE* const out, struct image* const img ) {
    const uint32_t width = (uint32_t) img->width;   
    const uint32_t height = (uint32_t) img->height;    
    const uint32_t padding = (uint32_t) get_padding(width);
    enum status status = write_header(out, width, height, padding);
    if (status == OK) {
        status = write_data(out, width, height, padding, img->data);
    }
    return status;
}
