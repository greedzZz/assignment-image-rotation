#include <bmp.h>
#include <file.h>
#include <logging.h>
#include <rotation.h>

int main( int argc, char** argv ) {
    if (argc != 3) {
        err(INPUT_ERR);
        return INPUT_ERR;
    }
    
    FILE* in = NULL;
    FILE* out = NULL;
    enum status open_status = open_file_read(argv[1], &in);
     
    if (open_status != OK) {
    	err(open_status);
    	return open_status;
    }
          
    open_status = open_file_write(argv[2], &out);
    
    if (open_status != OK) {
        fclose(in);
    	err(open_status);
    	return open_status;
    } 
    
    struct image old_img = {0};
    enum status read_status = from_bmp(in, &old_img);
    
    if (read_status != OK) {
        fclose(in);
        fclose(out);
        image_free_data(&old_img);
    	err(read_status);
    	return read_status;
    }
    
    struct image new_img = rotate(old_img);
    enum status write_status = to_bmp(out, &new_img);
    image_free_data(&new_img);
    image_free_data(&old_img);
    fclose(in);
    fclose(out);
    return write_status;   
}
