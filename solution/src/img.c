#include "img.h"

struct image image_create(uint64_t width, uint64_t height) {
    return (struct image) {.width = width, .height = height, .data = malloc(sizeof(struct pixel) * width * height)}; 
}

void image_free_data(struct image* const img) {
    free(img->data);
}
