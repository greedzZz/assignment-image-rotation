#include "file.h"

enum status open_file_read(char* const filename, FILE** file) {
	*file = fopen(filename, "rb");
	if (*file) return OK;
	return OPEN_ERR;
} 

enum status open_file_write(char* const filename, FILE** file) {
	*file = fopen(filename, "wb");
	if (*file) return OK;
	return OPEN_ERR;
}
