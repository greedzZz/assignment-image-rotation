#include "img.h"

static uint64_t calculate_x(size_t curr_y, uint64_t height){
    return height - 1 - curr_y;
}

static uint64_t calculate_y(size_t curr_x){
    return curr_x;
}

struct image rotate(struct image const source) {
    uint64_t width = source.width;
    uint64_t height = source.height;
    uint64_t size = width * height;
    struct pixel* old_data = source.data;
    struct image img = image_create(height, width);
    for (size_t i = 0; i < size; i++) {	
        uint64_t new_x = calculate_x(i / width, height);	
	uint64_t new_y = calculate_y(i % width);		
	img.data[new_x + new_y * height] = old_data[i];
    }
    return img;
}
