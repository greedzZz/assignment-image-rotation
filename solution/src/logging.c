#include <logging.h>

static const char* status_messages[] = {
    [OK] = "Successfully.\n",
    [INPUT_ERR] = "Incorrect input format.\n",
    [OPEN_ERR] = "A file was not opened successfully.\n",
    [WRITE_ERR] = "Data was not written successfully.\n",
    [READ_ERR] = "Data was not successfully read.\n",
    [READ_MALLOC_ERR] = "Malloc error.\n",
    [READ_FSEEK_ERR] = "Fseek error.\n",
    [READ_INVALID_TYPE] = "Invalid type.\n",
    [READ_INVALID_BPP] = "Invalid bits per pixel.\n",
    [READ_INVALID_HEADER_SIZE] = "Invalid header size.\n",
    [READ_INVALID_SIZE] = "Invalid image size.\n"   
};

void err(enum status stat) {
    const char* msg = status_messages[stat];
    fprintf(stderr, "%s", msg);
}
