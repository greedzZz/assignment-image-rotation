#ifndef BMP
#define BMP

#include <img.h>
#include <file.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

enum status from_bmp( FILE* const in, struct image* const img );

enum status to_bmp( FILE* const out, struct image* const img );
#endif
