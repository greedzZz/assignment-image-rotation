#ifndef ROTATION
#define ROTATION
#include <img.h>

struct image rotate( struct image const source );
#endif
