#ifndef FILE_H
#define FILE_H

#include <stdio.h>

enum status {
	OK = 0,
	INPUT_ERR,
	OPEN_ERR,
	WRITE_ERR,
    	READ_ERR,
    	READ_MALLOC_ERR,
    	READ_FSEEK_ERR,
    	READ_INVALID_TYPE,
    	READ_INVALID_BPP,
    	READ_INVALID_HEADER_SIZE,
    	READ_INVALID_SIZE   	 	
};

enum status open_file_read(char* const filename, FILE** file);

enum status open_file_write(char* const filename, FILE** file);
#endif
