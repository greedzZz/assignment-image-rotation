#ifndef IMG
#define IMG

#include <stdlib.h>
#include <stdint.h>

struct pixel { uint8_t b,g,r; };
struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct image image_create(uint64_t const width, uint64_t const height);

void image_free_data(struct image* const img);
#endif
